using System.Collections.Generic;

public interface IDeque<T> : IEnumerable<T>
{
    void AddFront(T value);

    void AddBack(T value);

    void RemoveFront();

    void RemoveBack();

    T SeekFront();

    T SeekBack();

    int Count { get; }
}