using System;
using System.Collections;
using System.Collections.Generic;

public class Deque<T> : IDeque<T>
{
    private const int INITIAL_CAPACITY = 4;

    private const int CAPACITY_MULTIPLIER = 2;

    private const string NO_ITEMS_ERROR_MESSAGE = "No items contained in the deque";

    private T[] items = new T[INITIAL_CAPACITY];

    private int frontIndex = 1;

    private int backIndex = 0;

    public int Count { get; private set; }

    public void AddBack(T value)
    {
        this.items[backIndex] = value;
        this.backIndex--;

        if (this.backIndex == -1)
        {
            this.backIndex = this.items.Length - 1;
        }

        if (this.backIndex == this.frontIndex)
        {
            this.Resize();
        }
    }

    public void AddFront(T value)
    {
        this.items[frontIndex] = value;
        this.frontIndex++;

        if (this.frontIndex == this.items.Length)
        {
            this.frontIndex = 0;
        }

        if (this.frontIndex == this.backIndex)
        {
            this.Resize();
        }
    }

    public void RemoveBack()
    {
        this.Count--;
        this.backIndex++;
    }

    public void RemoveFront()
    {
        this.Count--;
        this.backIndex--;
    }

    public T SeekBack()
    {
        if (this.Count == 0)
        {
            throw new Exception(NO_ITEMS_ERROR_MESSAGE);
        }

        return this.items[this.backIndex - 1];
    }

    public T SeekFront()
    {
        if (this.Count == 0)
        {
            throw new Exception(NO_ITEMS_ERROR_MESSAGE);
        }

        return this.items[this.frontIndex - 1];
    }

    public IEnumerator<T> GetEnumerator()
    {
        for (int i = 0; i < frontIndex; i++)
        {
            yield return this.items[i];
        }

        for (int i = frontIndex; i < items.Length; i++)
        {
            yield return this.items[i];
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

    private void Resize()
    {
        T[] newItems = new T[this.items.Length * CAPACITY_MULTIPLIER];

        for (int i = 0; i < items.Length; i++)
        {
            newItems[i] = items[i];
        }

        backIndex = 0;
        frontIndex = this.items.Length;
        this.items = newItems;
    }
}
