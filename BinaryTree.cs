using System;
using System.Collections.Generic;

public interface IBinaryTree<T> : IEnumerable<T>
{
    IBinaryTree<T> Left { get; set; }

    IBinaryTree<T> Right { get; set; }

    void Add(T value);

    bool Remove(T value);

    void Traverse(Action<T> action);
}